class ProceduresController < ApplicationController
  def index
    @procedures = Procedure.all

    if params[:name].present?
      @procedures = Procedure.match_begin_name(params[:name])
      @procedures += Procedure.match_tail_name(params[:name]).where.not(id: @procedures.map(&:id))
    end

    render json: ProcedureSerializer.render(@procedures), status: :ok
  end
end
