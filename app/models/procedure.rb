class Procedure < ApplicationRecord
  validates :name, presence: true

  scope :match_begin_name, ->(str) { where('name ILIKE ?', "#{str}%") }
  scope :match_tail_name, ->(str) { where('name ILIKE ?', "_%#{str}%") }
end
