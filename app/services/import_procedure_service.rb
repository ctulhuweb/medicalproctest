require 'open-uri'

class ImportProcedureService
  class << self
    def call
      procedures_names = prepare_data
      Procedure.transaction do
        procedures_names.each do |name|
          Procedure.find_or_create_by(name: name)
        end
      end
    end

    def prepare_data
      url = 'https://en.wikipedia.org/wiki/Medical_procedure'
      @doc = Nokogiri.HTML(URI.open(url))
      @doc.css('h3 + ul li').map { |c| c.children.map(&:text).join.split("\n")[0] }.flatten
    end
  end
end
