namespace :db do
  desc 'Import procedures'
  task import_procedures: :environment do
    ImportProcedureService.call
  end
end
