require 'rails_helper'

RSpec.describe Procedure, type: :model do
  it 'valid with valid attributes' do
    attrs = attributes_for(:procedure)
    expect(described_class.new(attrs)).to be_valid
  end

  it 'invalid without name' do
    attrs = attributes_for(:procedure, name: nil)
    expect(described_class.new(attrs)).not_to be_valid
  end
end
