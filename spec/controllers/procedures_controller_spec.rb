require 'rails_helper'

RSpec.describe ProceduresController, type: :request do
  describe 'GET #index' do
    subject(:get_procedures) { get procedures_path, params: params }

    let(:params) { { } }
    let(:procedures_count) { 2 }
    let(:json) { JSON.parse(response.body) }

    it 'return procedures' do
      create_list(:procedure, procedures_count)
      get_procedures

      expect(json.size).to eq(procedures_count)
    end

    context 'with name param' do
      let(:params) { { name: 'Anest' } }

      before do
        create_list(:procedure, procedures_count, name: 'Another anesthesia')
      end

      it 'procedures first by begin name' do
        first_procedure = create(:procedure, name: 'Anesthesia')
        get_procedures

        expect(json.first['id']).to eq(first_procedure.id)
      end

      it 'filter procedures by name' do
        create(:procedure)
        get_procedures

        expect(json.size).to eq(procedures_count)
      end
    end
  end
end
