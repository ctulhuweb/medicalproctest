require 'rails_helper'

RSpec.describe ImportProcedureService do
  it 'create procedures' do
    VCR.use_cassette('wiki/procedures') do
      expect { described_class.call }.to change { Procedure.count }
    end
  end
end
